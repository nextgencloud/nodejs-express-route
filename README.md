### 프로젝트 초기화
저장소를 복제후 최초 한번 프로젝트를 초기화 한다.
```bash
npm install express --save
```

## 
https://www.youtube.com/watch?v=pKd0Rpw7O48

### 다운로드
http://www.npmjs.org
expressjs.com
```bash
npm init --yes
npm i express
```


소스 변경시 핫 디플로이 기능   
npm install 명령 대신에 npm i 로 사용할 수 있다.  
nodemon은 핫디플로이 기능이다. node 로 시작할 경우 소스가 변경되면 다시 실행해야 변경된 소스가 반영된다. 그러나 nodemon은 소스 변경시 실시간 적용되어 node를 재기동 할 필요가 없다. 

```bash
npm i nodemon
nodemon app.js
```
크롬 확장 프로그램 추가
postman

https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop

mac install
https://www.getpostman.com/downloads/


#### valication
```bash
npm joi 검색

npm i joi

npm i joi@13.1.0 //안정적인 버전
```
joi는 validation을 제공하는 컴포넌트이다. schema를 선언하고 joi.validate에 스키마와 검증할 폼을 입력하면 주어진 조건에 맞게 validation 기능을 제공한다.
```javascript
function validateCourse(course){
    const schema = {
        name : Joi.string().min(3).required()
    };
    const result = Joi.validate(course, schema);
    console.log(result);

    return result;
}
```
